const elastic = require('../elastic');

defaultObject = () => {
    return {
        index:"restaurant",
        type:"type_restaurant",
    }
}

module.exports = {
    async Post(req, res){
        let obj = defaultObject();
        obj.body = req.body

        elastic.index(obj, (err) => {
            if(err){
                return res.status(500).json({msg:"There`s a error"})
            }else{
                return res.status(200).json({msg:"Added items"})
            }
        })
    },
    async Get(req, res){
        let obj = defaultObject();
        obj.id = req.body.id;

        elastic.get(obj, (err, response, status) => {
            if(err){
                return res.status(500).json({ msg: "There`s a error: " +err })
            }else{
                return res.status(200).json({ item: response._source })
            }
        })
    }
}