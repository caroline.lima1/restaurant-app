const express = require('express');
const router = express.Router();
const restaurantController = require('./controllers/RestaurantController');

router.post('/restaurants', restaurantController.Post)
router.get('/restaurants', restaurantController.Get)


module.exports = router;