const express = require('express');
const bodyParser = require('body-parser');
const router = require('./router');
const app = express();

var apm = require('elastic-apm-node').start({

    serviceName: 'restaurant-app',
  
    secretToken: 'restaurant123',
  
    serverUrl: 'http://localhost:8200'
  })


app.use(bodyParser.json());
app.use(router);

app.listen(4040, () => console.log('running'))